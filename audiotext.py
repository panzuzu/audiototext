# importing libraries 
import speech_recognition as sr 
import os
import requests
import time
from pydub import AudioSegment
from pydub.silence import split_on_silence
from pathlib import Path

########################################################################################################
# Programa para obtener texto de audio                                                                 #
# Funcionalidad de división de audio extraída de:                                                      #
# https://www.thepythoncode.com/article/using-speech-recognition-to-convert-speech-to-text-python      #
########################################################################################################

# Colores para letras
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Objeto de recognizer
r = sr.Recognizer()

# Función para extraer texto del audio
# Data Entry: path <- Archivo a convertir
# Return: path <- Archivo con texto de audio
def getTextFromAudio(path):
    # Abrimos audio y segmentamos mediante los silencios
    sound = AudioSegment.from_wav(path)
    chunks = split_on_silence(sound,
        # Valor de reconocimiento de silenci ** modificable **
        min_silence_len = 500,
        # Valor de ruido ** modificable **
        silence_thresh = sound.dBFS-14,
        # Espera de silencio ** modificable **
        keep_silence=500,
    )
    folder_name = "./tmp/audio-chunks"
    # Se crea directorio en caso de no estar mediante el nombre designado en folder_name
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    whole_text = ""
    # Generación de fragmentos de audios
    for i, audio_chunk in enumerate(chunks, start=1):
        # Se crean los audios en la carpeta
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        # Utilizamos speech_recognition para reconocer el audio
        with sr.AudioFile(chunk_filename) as source:
            audio_listened = r.record(source)
            # Convertimos a texto, mostramos por pantalla cada resultado
            # Agregamos el texto obtenido a la variable whole_text
            # Error: se muestra la exception
            try:
                text = r.recognize_google(audio_listened, language="es-ES")
            except sr.UnknownValueError:
                print(f"{bcolors.FAIL}- Error:{bcolors.ENDC}", "No se reconoce parte del audio.")
            except sr.RequestError:
                print(f"{bcolors.FAIL}- Error:{bcolors.ENDC}", "Problemas de conectividad con servidor de Google")
            else:
                text = f"{text.capitalize()}. \n"
                print(f"{bcolors.OKGREEN}- OK {bcolors.ENDC}{bcolors.UNDERLINE}", chunk_filename, f":{bcolors.ENDC}", text)
                whole_text += text

    # Obtenemos el nombre del archivo a convertir
    name_path = Path(path).stem

    # Creamos archivo de texto. 
    # Si encontramos un archivo con el mismo nombre agregamos milisegundos
    if not os.path.isfile('./txt/%s_audiototext.txt' % name_path):
        f = open ('./txt/%s_audiototext.txt' % name_path,'x')
    else:
        f = open ('./txt/%s_audiototext_%s.txt' % (name_path, time.time_ns()),'x')
    f.write(whole_text)
    f.close()

    # Imprimimos el texto en conjunto
    print(whole_text)

    # Retornamos el nombre del archivo texto generado
    return f.name


if __name__ == '__main__':

    print(f"""
    {bcolors.BOLD}################{bcolors.ENDC}
    {bcolors.BOLD}#{bcolors.ENDC}  {bcolors.OKCYAN}Audio{bcolors.ENDC}{bcolors.WARNING}To{bcolors.ENDC}{bcolors.OKGREEN}Text{bcolors.ENDC}{bcolors.BOLD} #{bcolors.ENDC}
    {bcolors.BOLD}################{bcolors.ENDC}
    """)

    url = "https://www.google.com"

    timeout = 5

    try:
        request = requests.get(url, timeout=timeout)
    except (requests.ConnectionError, requests.Timeout) as exception:
        print(f"{bcolors.FAIL}-Error:{bcolors.ENDC} {bcolors.BOLD}Este programa necesita conexión a red para la conexión con el servicio de Google.{bcolors.ENDC}")
        quit()


    # Usuario debe ingresar la ruta del archivo 
    print(f"{bcolors.WARNING}Ingresar la ruta del archivo a convertir:{bcolors.ENDC}")
  
    path = input()

    # Comprobamos que existe el archivo
    if not os.path.isfile(path):
        print(f"{bcolors.FAIL}- Error: No se encontró el archivo ingresado{bcolors.ENDC}")
        quit()

    # Checkeamos su extensión
    # Si es MP3 o M4A convertimos

    if path.endswith('.mp3'):
        print(f"{bcolors.WARNING}- Archivo MP3: Convirtiendo MP3 en WAV...{bcolors.ENDC}")
        dst = "./tmp/convert-audios/%s.wav" % time.time_ns()                                                          
        audSeg = AudioSegment.from_mp3(path)
        audSeg.export(dst, format="wav")
        print(dst)
        path = dst
    if path.endswith('.m4a'):
        print(f"{bcolors.WARNING}- Archivo M4A: Convirtiendo M4A en WAV...{bcolors.ENDC}")
        dst = "./tmp/convert-audios/%s.wav" % time.time_ns()                                                          
        audSeg = AudioSegment.from_file(path)
        audSeg.export(dst, format="wav")
        print(dst)
        path = dst
    if path.endswith('.ogg'):
        print(f"{bcolors.WARNING}- Archivo OGG: Convirtiendo OGG en WAV...{bcolors.ENDC}")
        dst = "./tmp/convert-audios/%s.wav" % time.time_ns()                                                          
        audSeg = AudioSegment.from_file(path)
        audSeg.export(dst, format="wav")
        print(dst)
        path = dst
    elif path.endswith('.wav'):
        print(f"{bcolors.OKGREEN}- OK Formato de archivo aceptado{bcolors.ENDC}")
    else:
        print(f"{bcolors.FAIL}- Error: El programa no soporta el formato dado.{bcolors.ENDC}")
        quit()
    
    # Llamamos a la función para obtener el texto
    print(f"\n{bcolors.OKGREEN}- OK{bcolors.ENDC}\n{bcolors.BOLD}El archivo con el texto fue guardado en{bcolors.ENDC}{bcolors.OKCYAN} %s{bcolors.ENDC}" % getTextFromAudio(path))

    # Eliminamos archivos basura
    dir = './tmp/audio-chunks'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))
    dir = './tmp/convert-audios'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))